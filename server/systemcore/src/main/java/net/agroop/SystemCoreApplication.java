package net.agroop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * SystemCoreApplication.java
 * Created by Rúben Madeira on 05/01/2018 - 22:52.
 * Copyright 2018 © eAgroop,Lda
 */
@EnableAspectJAutoProxy
@SpringBootApplication
public class SystemCoreApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SystemCoreApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SystemCoreApplication.class, args);
    }
}